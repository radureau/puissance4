# -*- coding: utf-8 -*-
class Joueur(object):
	"""
	Pour commencer une partie il faut deux joueurs.
	On peut mémoriser diverse données sur le joueur
	"""
	
	def __init__(self,nom="Joueur"):
		self.nom	=	nom
	
	def jouer(self, grille, jeton):
		grille.jouer(jeton)
	
	def peutJouer(self, grille):
		return grille.joueur_peut_jouer(self)
	
	def __eq__(self, objet):
		if not isinstance(objet,Joueur):
			return False
		return self.nom == objet.nom
