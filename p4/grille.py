# -*- coding: utf-8 -*-
import json
from .jeton import Jeton

class Grille(object):
	"""
	La grille est une matrice de Jetons de dimmensions 6x7
	"""
	
	def __init__(self,joueur1=None,joueur2=None,max_row=6,max_column=7, grille=None, nbTour=0):
		self.joueur1	=	joueur1
		self.j1Gagne	=	False
		self.joueur2	=	joueur2
		self.j2Gagne	=	False
		self.max_row	=	max_row
		self.max_column	=	max_column
		self.grille		=	grille if grille is not None else list()
		self.nbTour		=	nbTour
		for n_row in range(max_row):
			self.grille.append([None]*max_column)
	
	def __repr__(self):
		string = str("\n")
		for row in self.grille:
			for cell in row:
				string+='['
				if cell == None:
					string+=' '
				elif cell.joueur == self.joueur1:
					string+=cell.repr_j1()
				else:
					string+=cell.repr_j2()
				string+=']'
			string+='\n'
		string+='\n'
		return string
	
	def __hash__(self):
		return hash(repr(self))
	
	def estRemplie(self):
		"""
		Indique si la grille a été complétement remplie.
		"""
		return len( [ cell for cell in self.grille[0] if cell==None] )==0
	
	def freeIColumns(self):
		return [i for i in range(self.max_column) if self.grille[0][i] is None]
	
	def getColumn(self, columnNumber):
		res=[]
		for row in self.grille:
			res.append(row[columnNumber])
		return res
	
	def setColumn(self, column, columnNumber):
		i=0
		for row in self.grille:
			row[columnNumber]=column[i]
			i+=1
	
	def getRow(self, rowNumber):
		return self.grille[rowNumber]
	
	def setRow(self, row, rowNumber):
		self.grille[rowNumber]=row
	
	def empiler(self, jeton):
		try:
			assert jeton.col != None
		except AssertionError:
			print("Il faut indiquer le numéro de colonne dans le jeton",jeton)
		column = self.getColumn(jeton.col)
		try:
			assert self.grille[0][jeton.col] == None
			
			i=self.max_row
			while jeton not in column:
				i+=1
				if column[self.max_row-i] == None:
					column[self.max_row-i] = jeton
			jeton.row=i
			self.setColumn(column, jeton.col)
			
		except AssertionError:
			print("La colonne est pleine",column)
			raise IndexError("La colonne est pleine")
	
	def jouer(self, jeton):
		"""
		Met à jour la grille
		démarche: quand le joueur joue on fait un grille.jouer(Jeton(row=?))
		voir exemples dans test_p4.py
		"""
		try:
			self.empiler(jeton)
			self.nbTour+=1
		except IndexError:
			print("impossible de jouer sur cette colonne")
	
	def chercherPuissance4(self,dir_x,dir_y,col,row,count=0,joueur=None):
		if row >= self.max_row or row<0 or col >= self.max_column or col<0:
			return False
		elif self.grille[row][col] == None:
			return False
		else:
			if count == 0:
				joueur = self.grille[row][col].joueur
				count+=1
			elif self.grille[row][col].joueur == joueur:
				count+=1
			else:
				return False
			if count == 4:
				if joueur==self.joueur1:
					self.j1Gagne=True
				else:
					self.j2Gagne=True
				return True
			else:
				return self.chercherPuissance4(dir_x, dir_y, col+dir_x, row+dir_y, count, joueur)
			
	
	def chercherNPuissance4(self,dir_x,dir_y):
		res=False
		for n_row in range(self.max_row):
			for n_col in range(self.max_column):
				res=res or self.chercherPuissance4(dir_x, dir_y, n_col, n_row)
		return res
	
	def puissance4(self):
		"""
		Retourne un booléen indiquant si au moins un puissance4 a été réalisé.
		"""
		res=False
		directions=[(0,1),
					(1,0),
					(1,1),
					(1,-1),
					]
		for dir_x, dir_y in directions:
			res=res or self.chercherNPuissance4(dir_x,dir_y)
		return res
	
	def joueur_peut_jouer(self, joueur):
		if self.estRemplie() or self.puissance4():
			return False
		if joueur == self.joueur1:
			return self.nbTour%2==0
		else:
			return self.nbTour%2==1
	
	def setJoueur1(self,joueur1):
		for row in self.grille:
			for cell in row:
				if cell is not None and cell.joueur==self.joueur1:
					cell.joueur = joueur1
		self.joueur1 = joueur1
	
	def setJoueur2(self,joueur2):
		for row in self.grille:
			for cell in row:
				if cell is not None and cell.joueur==self.joueur2:
					cell.joueur = joueur2
		self.joueur2 = joueur2
	
	def clone(self):
		import copy
		return copy.deepcopy(self)
	
	def __eq__(self, objet):
		res = isinstance(objet,Grille)
		res = res and objet.nbTour == self.nbTour
		res = res and objet.joueur1 == self.joueur1
		res = res and objet.joueur2 == self.joueur2
		res = res and objet.max_row == self.max_row
		res = res and objet.max_column == self.max_column
		res = res and objet.grille == self.grille
		return res
