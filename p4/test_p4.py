# -*- coding: utf-8 -*-
from joueur import Joueur
from jeton import Jeton
from grille import Grille

def test_initJoueur():
	joueur = Joueur()
	joueur = Joueur("Michael")
	assert joueur.nom=="Michael"

def test_initJeton():
	joueur = Joueur("Michael")
	jeton = Jeton(joueur=joueur)
	assert jeton.joueur.nom == "Michael"

def test_initGrille():
	grille = Grille()
	
	var=[[None]*7,[None]*7,[None]*7,[None]*7,[None]*7,[None]*7]
	
	assert grille.grille == var

def test_jouerJeton():
	joueur1 = Joueur("Player 1")
	joueur2 = Joueur("Player 2")
	grille = Grille(joueur1=joueur1, joueur2=joueur2)
	var	=	[[None]*7,[None]*7,[None]*7,[None]*7,[None]*7,[None]*7]
	assert grille.grille == var
	jeton = Jeton(joueur=joueur1, col=3)
	joueur1.jouer(grille, jeton)
	var[-1][3] = jeton
	print(grille)
	assert grille.grille == var
	jeton = Jeton(joueur=joueur2, col=3)
	joueur2.jouer(grille, jeton)
	print(grille)
	print(var)
	var[-2][3] = jeton
	assert grille.grille == var

def test_grilleEstRemplie():
	grille = Grille()
	assert grille.estRemplie() == False
	joueur = Joueur()
	i=0
	for k in range(7):
		for j in range(6):
			joueur.jouer(grille, Jeton(joueur=joueur, col=i))
		i+=1
	assert grille.estRemplie() == True


def test_joueurPeutJouer():
	joueur1 = Joueur("Player 1")
	joueur2 = Joueur("Player 2")
	grille = Grille(joueur1=joueur1, joueur2=joueur2)
	assert joueur2.peutJouer(grille) == False
	assert joueur1.peutJouer(grille) == True
	jeton = Jeton(joueur=joueur1, col=3)
	joueur1.jouer(grille, jeton)
	assert joueur2.peutJouer(grille) == True
	assert joueur1.peutJouer(grille) == False
	joueur1.jouer(grille, Jeton(joueur=joueur1, col=3))
	joueur1.jouer(grille, Jeton(joueur=joueur1, col=3))
	joueur1.jouer(grille, Jeton(joueur=joueur1, col=3))
	assert joueur2.peutJouer(grille) == False
	assert joueur1.peutJouer(grille) == False

def test_puissance4Vertical():
	joueur1 = Joueur("Player 1")
	joueur2 = Joueur("Player 2")
	grille = Grille(joueur1=joueur1, joueur2=joueur2)
	assert grille.puissance4() == False
	for k in range(4):
		jeton = Jeton(joueur=joueur1, col=3)
		joueur1.jouer(grille, jeton)
	print(grille)
	assert grille.puissance4() == True

def test_puissance4Horizontal():
	joueur1 = Joueur("Player 1")
	joueur2 = Joueur("Player 2")
	grille = Grille(joueur1=joueur1, joueur2=joueur2)
	assert grille.puissance4() == False
	for k in range(4):
		jeton = Jeton(joueur=joueur1, col=k)
		joueur1.jouer(grille, jeton)
	print(grille)
	assert grille.puissance4() == True

def test_puissance4DiagonaleGauche():
	joueur1 = Joueur("Player 1")
	joueur2 = Joueur("Player 2")
	grille = Grille(joueur1=joueur1, joueur2=joueur2)
	assert grille.puissance4() == False
	jeton = Jeton(joueur=joueur1)
	row2=[jeton,None,None,None,None,None,None]
	row3=[None,jeton,None,None,None,None,None]
	row4=[None,None,jeton,None,None,None,None]
	row5=[None,None,None,jeton,None,None,None]
	grille.setRow(row2,2)
	grille.setRow(row3,3)
	grille.setRow(row4,4)
	assert grille.puissance4() == False
	grille.setRow(row5,5)
	print(grille)
	assert grille.puissance4() == True

def test_puissance4DiagonaleDroite():
	joueur1 = Joueur("Player 1")
	joueur2 = Joueur("Player 2")
	grille = Grille(joueur1=joueur1, joueur2=joueur2)
	assert grille.puissance4() == False
	jeton = Jeton(joueur=joueur1)
	row2=[None,None,None,None,None,None,jeton]
	row3=[None,None,None,None,None,jeton,None]
	row4=[None,None,None,None,jeton,None,None]
	row5=[None,None,None,jeton,None,None,None]
	grille.setRow(row2,2)
	grille.setRow(row3,3)
	grille.setRow(row4,4)
	assert grille.puissance4() == False
	grille.setRow(row5,5)
	print(grille)
	assert grille.puissance4() == True
