# -*- coding: utf-8 -*-
from .joueur import Joueur

class Jeton:
	"""
	Un jeton appartient à un joueur et connait sa position dans la grille
	"""
	
	def __init__(self,joueur=None,col=None, row=None):
		self.joueur	=	joueur
		self.col	=	col
		self.row	=	row
	
	def repr_j1(self):
		return "X"
	
	def repr_j2(self):
		return "O"
	
	def __eq__(self, objet):
		res = isinstance(objet,Jeton)
		return res and self.joueur == objet.joueur and self.col == objet.col and self.row == objet.row
