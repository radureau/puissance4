#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import sys, subprocess, os, pickle
isWindows = 'win' in sys.platform
ahead = 2 if len(sys.argv) == 1 else int(sys.argv[1])

from ia import GameManager
from p4 import Joueur

manager = GameManager()

username = subprocess.check_output("whoami").decode().strip()
if isWindows:
	username = username.split('\\')[-1]
human = Joueur(username)
print("Good luck %s." % (human.nom,))
if isWindows:
	os.chdir("\\Users\\%s" % (human.nom,))
else:
	os.chdir("/home/%s" % (human.nom,))

scores={'V':0, 'D':0, 'T': 0}
try:
	with open('.p4log', 'rb') as scoresLog:
		depickler = pickle.Unpickler(scoresLog)
		scores = depickler.load()
except:
	pass

from random import randint
if randint(0,1) == 0:
	manager.initGame(joueur1=human, ahead=ahead)
	humanHasToPlay=True
else:
	manager.initGame(joueur2=human, ahead=ahead)
	humanHasToPlay=False

while not manager.game.isDone():
	try:
		if humanHasToPlay:
			print(manager.game)
			print("[0][1][2][3][4][5][6]")
			manager.game.play(move= int(input()) )
		humanHasToPlay = not humanHasToPlay
	except (KeyboardInterrupt, SystemExit):
		 #raise
		 sys.exit()
	except:
		pass
print(manager.game)

if manager.game.playerHasWon(human):
	scores['V']+=1
	print("One more victory over the machine for %s!" %(human.nom,))
elif manager.game.playerHasLost(human):
	scores['D']+=1
	print("Well, technology is supposed to best even you, %s." %(human.nom,))
else:
	scores['T']+=1
	print("It's a draw! I wonder who was the luckiest this time, %s or the machine?" % (human.nom,))

with open('.p4log', 'wb') as scoresLog:
	pickler = pickle.Pickler(scoresLog)
	pickler.dump(scores)
	print("You won %d times, lost %d times and played %d games." % (scores['V'], scores['D'], sum(scores.values()),))
