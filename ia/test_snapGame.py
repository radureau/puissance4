# -*- coding: utf-8 -*-
from ia import SnapGame, Connect4, GameManager
from p4 import Joueur, Jeton

def test_SnapGameManager():
	joueur1 = Joueur("P1")
	joueur2 = Joueur("P2")
	manager = GameManager()
	manager.initGame(joueur1, joueur2)
	snap1 = manager.snapManager.snap1
	manager.game.play(move=0)
	p4Game = Connect4(joueur1,joueur2)
	from copy import deepcopy
	_ = manager.game
	p4Game.play(move=0)
	assert p4Game == manager.game
	p4Game.play(move=3)
	manager.game.play(move=3)
	assert snap1.etat != p4Game
	_p4 = deepcopy(p4Game)
	snap2 = manager.snapManager.snap2
	assert snap2.etat != _p4
	

def test_Connect4_possibleMoves():
	joueur = Joueur("Player 1")
	ia = Joueur("PC 2")
	p4Game = Connect4(player2=ia, player1=joueur)
	jton = Jeton(joueur=ia)
	blok = Jeton(joueur=joueur)
	row0=[None,None,blok,blok,None,None,None]
	p4Game.setRow(row0,0)
	snap=SnapGame(etat=p4Game,player=ia)
	snap.calculateAhead(n=2)
	assert p4Game.possibleMoves()==[0,1,4,5,6]
	assert p4Game.possibleMoves()==snap.bestMoves()

def test_Connect4_copy():
	game = Connect4()
	copy = game.copy()
	assert copy != game
	game.play(3)
	assert copy.grille != game.grille
	game.play(4)
	game.play(3)
	game.play(4)
	game.play(3)
	game.play(3)
	copy.play(6)
	copy.play(6)
	copy.play(6)

def test_SnapGame():
	p4Game = Connect4()
	snap = SnapGame(etat=p4Game,player=p4Game.joueur1)
	snap.calculateNextSnaps()
	indiceColonneDuMilieu = 3
	move = indiceColonneDuMilieu
	assert snap.snapsSuivants[move][move].snapsSuivants == None
	snap2 = SnapGame(etat=p4Game,player=p4Game.joueur2)
	snap2.calculateAhead(n=2)
	assert snap.bestMoves() == snap2.bestMoves()

def test_bestMoves():
	joueur = Joueur("Player 1")
	ia = Joueur("PC 2")
	p4Game = Connect4(player2=ia, player1=joueur)
	jton = Jeton(joueur=ia)
	blok = Jeton(joueur=joueur)
	row2=[None,None,None,None,None,None,None]
	row3=[None,None,None,blok,None,None,None]
	row4=[None,None,jton,blok,None,None,None]
	row5=[None,None,jton,blok,None,None,None]
	p4Game.setRow(row2,2)
	p4Game.setRow(row3,3)
	p4Game.setRow(row4,4)
	p4Game.setRow(row5,5)
	p4Game.nbTour=5
	assert ia.peutJouer(p4Game)
	snap = SnapGame(etat=p4Game, player=ia)
	snap.calculateNextSnaps()
	print(snap._move_totalPoints(2))
	assert snap.bestMoves()==[3]
	from random import choice
	snap.etat.play(move=choice(snap.bestMoves()))
	assert not snap.etat.isDone()

def test_GameManager_Joueur_Vs_Pc():
	manager = GameManager()
	manager.initGame(joueur1=Joueur("Player 1"))
	manager.game.play(move=3)
	manager.game.play(move=3)
	manager.game.play(move=3)
	manager.game.play(move=3)
	game=manager.game
	assert manager.game.isDone() is False

def test_GameManager_Pc_Vs_Pc():
	manager = GameManager()
	manager.initGame()
	assert manager.game.isDone()

def test_GameManager_Pc_Vs_Joueur():
	manager = GameManager()
	manager.initGame(joueur2=Joueur("Player 1"))
	manager.game.play(move=3)
	manager.game.play(move=3)
	manager.game.play(move=3)
	manager.game.play(move=3)
	game=manager.game

def test_BestMoves_CertainKill():
	joueur = Joueur("Player 1")
	ia = Joueur("PC 2")
	p4Game = Connect4(player2=ia, player1=joueur)
	jton = Jeton(joueur=ia)
	blok = Jeton(joueur=joueur)
	row4=[None,None,jton,jton,None,None,None]
	row5=[None,None,blok,blok,None,None,None]
	p4Game.setRow(row4,4)
	p4Game.setRow(row5,5)
	p4Game.nbTour=5
	assert ia.peutJouer(p4Game)
	snap = SnapGame(etat=p4Game, player=ia)
	snap.calculateAhead(n=2)
	for ownMove in snap.snapsSuivants.values():
			for consequence in ownMove.values():
				print(consequence.etat)
	assert snap._move_totalPoints(1) == snap._move_totalPoints(4)
	assert snap._move_totalPoints(1) > snap._move_totalPoints(2)
	assert snap.bestMoves()==[1,4]
