# -*- coding: utf-8 -*-
from ia import SnapGame
from random import choice
class SnapGameManager():
	
	def __init__(self, game, player1IsHuman=False, player2IsHuman=False, ahead=2):
		self.game = game
		self.ahead = ahead
		self.snap1 = SnapGame(etat=self.game.copy(),player=self.game.joueur1)
		self.snap1.calculateAhead(n= self.ahead if not player1IsHuman else 1)
		self.game.addListener(self)
		self.player1IsHuman = player1IsHuman
		self.snap2 = None
		self.player2IsHuman = player2IsHuman
		self.wasPlayer1Turn=True
		if not self.player1IsHuman:
			self.automatic_play(self.snap1)
	
	def notify(self, kw):## appelée lorsqu'un tour a été fait
		move = kw["move"]
		if self.wasPlayer1Turn:
			self.snap1.notice(move)
			if self.snap2 == None:
				self.snap2 = SnapGame(etat=self.game.copy(),player=self.game.joueur2)
			else:
				self.snap2.notice_back(move)
				self.snap2 = self.snap2.getNextSnap()
			self.snap2.calculateAhead(n= self.ahead if not self.player2IsHuman else 1)
			if self.player2IsHuman is False:
				self.wasPlayer1Turn = not self.wasPlayer1Turn
				self.automatic_play(self.snap2)
			else:
				self.wasPlayer1Turn = not self.wasPlayer1Turn
		else:
			self.snap2.notice(move)
			self.snap1.notice_back(move)
			self.snap1 = self.snap1.getNextSnap()
			self.snap1.calculateAhead(n= self.ahead if not self.player1IsHuman else 1)
			if self.player1IsHuman is False:
				self.wasPlayer1Turn = not self.wasPlayer1Turn
				self.automatic_play(self.snap1)
			else:
				self.wasPlayer1Turn = not self.wasPlayer1Turn
	
	def automatic_play(self,snap):
		if not self.game.isDone():
			self.game.play(move=choice(snap.bestMoves()))
