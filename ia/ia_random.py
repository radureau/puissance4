from p4 import Joueur, Jeton
from random import randint
class IARandom(Joueur):
	def jouer(self, grille):
		while self.peutJouer(grille):
			grille.jouer(Jeton(joueur=self, col=randint(0,grille.max_column)))
