# -*- coding: utf-8 -*-
from ia import Connect4, SnapGame, SnapGameManager
class GameManager:
	
	def __init__(self):
		self.game = None
	
	def initGame(self, joueur1=None, joueur2=None, ahead=2):
		self.game = Connect4(joueur1, joueur2)
		self.snapManager = SnapGameManager( game=			self.game,
											player1IsHuman=	(joueur1 is not None),
											player2IsHuman=	(joueur2 is not None),
											ahead=			ahead,
											)
