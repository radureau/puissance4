# -*- coding: utf-8 -*-
class SnapGame:
	
	totalSnap=0
	def __init__(self, etat, player=None, snapPrecedant=None, points=0, snapsSuivants=None, played=0, victories=0, loss=0):
		self.id = SnapGame.totalSnap
		SnapGame.totalSnap+=1
		
		self.played = played
		self.victories = victories
		self.loss = loss
		self.etat = etat
		self.snapPrecedant = snapPrecedant
		self.points = points
		self.snapsSuivants = snapsSuivants
		self.player = player if player is not None else self.etat.currentPlayer()
		if self.points == 0 and self.etat is not None:
			self.updatePoints()
	
	def __repr__(self):
		return "<id:"+str(self.id)+" pts: "+str(self.points)+"| "+str(self.snapsSuivants)+">"
	
	def __eq__(self, other):
		if isinstance(other, SnapGame):
			return self.etat == other.etat
		return False
	
	def calculateNextSnaps(self):
		if self.snapsSuivants is None:
			self.snapsSuivants = dict()
			for move in self.etat.possibleMoves():#possibleMoves() should be implemented
				simulation = self.etat.copy()
				simulation.play(move, notify=False)#play(move, notify=True) should be implemented
				listOfConsequences = dict()
				for opponent_move in simulation.possibleMoves():
					consequence = simulation.copy()
					consequence.play(opponent_move, notify=False)
					listOfConsequences[opponent_move] = SnapGame(etat=consequence,
														player=self.player,
														snapPrecedant=self,
														)
				self.snapsSuivants[move] = listOfConsequences
	
	def profondeur(self):
		if self.snapsSuivants is None:
			return 0
		l=[]
		for ownMove in self.snapsSuivants.values():
			for consequence in ownMove.values():
				l.append(consequence.profondeur())
		return max(l)+1
		
	def calculateAhead(self, n=1):
		if n>0:
			self.calculateNextSnaps()
			self._calculateAhead(n=n-1)
	def _calculateAhead(self, n):
		if n>0:
			for ownMove in self.snapsSuivants.values():
				for consequence in ownMove.values():
					consequence.calculateAhead(n)
	
	def updatePoints(self):
		if self.etat.isDone():#isDone() should be implemented
			if self.etat.playerHasWon(player=self.player):#playerHasWon() should be implemented
				currentSnap=self
				while currentSnap is not None:
					currentSnap.points+=1
					currentSnap.victories+=1
					currentSnap=currentSnap.snapPrecedant
			elif self.etat.playerHasLost(player=self.player):#playerHasLost() should be implemented
				currentSnap=self
				while currentSnap is not None:
					currentSnap.points+=-1
					currentSnap.loss+=1
					currentSnap=currentSnap.snapPrecedant
	
	def _move_totalPoints(self,move):
		return sum([snap.points for snap in self.snapsSuivants[move].values()])
	
	def _max_points(self):
		return max([self._move_totalPoints(move) for move in self.snapsSuivants])
	
	def _is_best_move(self,move):
		return self._move_totalPoints(move) == self._max_points()
	
	def bestMoves(self):
		moves=list()
		#self.calculateNextSnaps()
		for move in self.snapsSuivants:
			if self._is_best_move(move):
				moves.append(move)
		return moves
	
	def notice(self, chosen_move):
		self.chosen_move = chosen_move
	
	def notice_back(self, opponent_move):
		self.opponent_move = opponent_move
	
	def getNextSnap(self):
		if self.opponent_move not in self.snapsSuivants[self.chosen_move]:
			print(self.snapsSuivants[self.chosen_move].keys())
		nextSnap = self.snapsSuivants[self.chosen_move][self.opponent_move]
		nextSnap.played += 1
		return nextSnap
