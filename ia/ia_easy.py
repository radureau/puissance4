from p4 import Jeton, Grille
from ia import IARandom
from random import randint
class IAeasy(IARandom):
	
	def strategie_easy(jouer):
		def faireOuBloquerPuissance4(self, grille):
			if self.peutJouer(grille):
				self.tenterPuissance4(grille)
			if self.peutJouer(grille):
				self.bloquerPuissance4(grille)
			jouer(self,grille)
		return faireOuBloquerPuissance4
	
	@strategie_easy
	def jouer(self, grille):
		super(IAeasy,self).jouer(grille)
		
	def tenterPuissance4(self,grille):
		g_test = grille
		col = 0
		while not g_test.puissance4() and col < grille.max_column:
			g_test=grille.clone()
			super(IARandom,self).jouer(g_test,Jeton(self,col))
			col+=1
		if g_test.puissance4():
			super(IARandom,self).jouer(grille,Jeton(self,col-1))
	
	def bloquerPuissance4(self,grille):
		g_test = grille
		
