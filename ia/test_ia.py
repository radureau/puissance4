from p4 import Grille, Joueur, Jeton
from ia import IARandom, IAeasy, IAGrille, Possibilites

def test_IARandom_init():
	assert IARandom("Michael").nom == "Michael"

def test_IARandom_jouer():
	ia = IARandom()
	grille = Grille(Joueur(),ia)
	ia.jouer(grille)

def test_IAeasy_init():
	assert IAeasy("Michael").nom == "Michael"

def test_IAeasy_jouer():
	ia = IAeasy("IA")
	grille = Grille(Joueur(),ia)
	ia.jouer(grille)

def test_IAeasy_finirPuissance4():
	joueur = Joueur("Player 1")
	ia = IAeasy("IA")
	grille = Grille(joueur1=ia, joueur2=joueur)
	jton = Jeton(joueur=ia)
	blok = Jeton(joueur=joueur)
	row2=[None,None,None,None,None,None,None]
	row3=[None,None,None,None,jton,jton,None]
	row4=[None,None,None,jton,blok,blok,None]
	row5=[None,None,jton,blok,blok,blok,None]
	grille.setRow(row2,2)
	grille.setRow(row3,3)
	grille.setRow(row4,4)
	grille.setRow(row5,5)
	print(grille)
	ia.jouer(grille)
	assert grille.puissance4() == True

def atest_IAeasy_Bloquer_Puissance4():
	joueur = Joueur("Player 1")
	ia = IAeasy("IA")
	
	grille = Grille(joueur1=ia, joueur2=joueur)
	jton = Jeton(joueur=ia)
	blok = Jeton(joueur=joueur)
	row2=[None,None,None,None,None,None,None]
	row3=[None,None,jton,blok,None,None,None]
	row4=[None,None,jton,blok,None,None,None]
	row5=[None,None,jton,blok,None,None,None]
	grille.setRow(row2,2)
	grille.setRow(row3,3)
	grille.setRow(row4,4)
	grille.setRow(row5,5)
	ia.jouer(grille)
	assert grille.puissance4() == True
	
	grille = Grille(joueur1=ia, joueur2=joueur)
	row2=[None,None,None,None,None,None,None]
	row3=[None,None,None,blok,None,None,None]
	row4=[None,None,jton,blok,None,None,None]
	row5=[None,jton,jton,blok,None,None,None]
	grille.setRow(row2,2)
	grille.setRow(row3,3)
	grille.setRow(row4,4)
	grille.setRow(row5,5)
	ia.jouer(grille)
	joueur.jouer(grille,Jeton(joueur=joueur,col=3))
	assert grille.puissance4() == False

def test_IAGrille_init():
	joueur1 = Joueur("J1")
	joueur2 = Joueur("J2")
	grille = Grille(joueur1=joueur1, joueur2=joueur2)
	arbre = IAGrille(grille, limite = 0)
	print("arbre.profondeur()",arbre.profondeur())
	print("arbre.nbEnfants()",arbre.nbEnfants())

def test_possibilites():
	generator = Possibilites()
	for tour in range(4):
		possible = generator.nextStep(generator.lastStep["step"],generator.lastStep["n"])
		for grille in possible:
			assert isinstance(grille,Grille)
