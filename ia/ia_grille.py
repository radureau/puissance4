# -*- coding: utf-8 -*-
from p4 import Jeton, Grille
class IAGrille(object):
	"""
	construit l'abre des possibilités de profondeur <limite>
	avec grille une instance de p4.Grille
	et jeton une instance de p4.Jeton qui indique la dernière 
	colonne jouée et par quel joueur
	"""
	
	def __init__(self,grille,jeton=None,limite=-1):
		"""
		construit l'abre des possibilités de profondeur <limite>
		avec grille une instance de p4.Grille
		et jeton une instance de p4.Jeton qui indique la dernière 
		colonne jouée et par quel joueur
		"""
		self.grille = grille.clone()
		self.jeton = jeton
		self.enfants = set()
		
		if jeton is not None:
			self.jeton.joueur.jouer(self.grille,self.jeton)
		
		if limite != 0 and not self.grille.estRemplie() and not self.grille.puissance4():
			for col in self.grille.freeIColumns():
				jeton = Jeton(joueur= self.grille.joueur1 if self.jeton is None or self.jeton.joueur == self.grille.joueur2 else self.grille.joueur2,
							col=col)
				self.enfants.add(IAGrille(grille,jeton,limite-1))
		
	def __eq__(self, objet):
		if isinstance(objet, IAGrille):
			return self.jeton.joueur == objet.jeton.joueur and self.jeton.col == objet.jeton.col and objet.grille == self.grille
		elif isinstance(objet, Grille):
			return objet == self.grille
		else:
			return None
	
	def estFeuille(self):
		return len(self.enfants)==0
	
	def profondeur(self):
		if self.estFeuille():
			return 0
		return max([ e.profondeur()+1 for e in self.enfants])
	
	def nbEnfants(self):
		if self.estFeuille():
			return 1
		return sum([ e.nbEnfants() for e in self.enfants])
