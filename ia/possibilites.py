# -*- coding: utf-8 -*-
from p4 import Joueur, Jeton, Grille
class Possibilites():
	
	def __init__(self):
		self.j1 = Joueur("j1")
		self.j2 = Joueur("j2")
		self.lastStep = {"step":None,"n":0}
	
	def nextStep(self,step=None,n=0):
		res=set()
		if step is None:
			res.add(Grille(joueur1=self.j1,joueur2=self.j2))
		else:
			for grille in step:
				if not ( grille.puissance4() or grille.estRemplie() ):
					for col in grille.freeIColumns():
						_grille = grille.clone()
						_grille.jouer(Jeton(joueur= self.j1 if n%2 == 0 else self.j2, col=col))
						res.add(_grille)
		self.lastStep["step"]=res
		self.lastStep["n"]=n+1
		return res
