from .app import manager, db
from ia import Possibilites

@manager.command
def loaddb():
	'''Met toutes les grilles possibles dans une BD.'''
	
	# création de toutes les tables
	from .models import Grille
	db.create_all()
	
	from yaml import dump
	generator = Possibilites()
	for tour in range(6*7):
		possible = generator.nextStep(generator.lastStep["step"],generator.lastStep["n"])
		for grille in possible:
			g = Grille(yaml=dump(grille),tour=tour,points=0)
			db.session.add(g)
		db.session.commit()
