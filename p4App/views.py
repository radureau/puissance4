from p4App import app
from flask import render_template, session
from p4 import Joueur, Jeton, Grille
from ia import IARandom

@app.route('/')
def home():
	if 'grille' in session:
		session.pop('grille')
	return render_template("home.html",
							title="Accueil"
							)

import jsonpickle

@app.route('/jouer/')
@app.route('/jouer/<int:col>')
def jouer(col=None):
	if 'grille' in session:
		grille=jsonpickle.decode(session['grille'])
		if (grille.joueur1.nom=="IA" or grille.joueur2.nom=="IA") or grille.puissance4() or grille.estRemplie():
			session.pop('grille')
		elif col is not None and col < grille.max_column:
			joueur1 = grille.joueur1
			joueur2 = grille.joueur2
			if joueur1.peutJouer(grille):
				joueur1.jouer(grille,Jeton(joueur1,col))
			elif joueur2.peutJouer(grille):
				joueur2.jouer(grille,Jeton(joueur2,col))
			session['grille']=jsonpickle.encode(grille)
	if 'grille' not in session:
		joueur1=Joueur("Player 1")
		joueur2=Joueur("Player 2")
		grille=Grille(joueur1=joueur1, joueur2=joueur2)
		session['grille']=jsonpickle.encode(grille)#, unpicklable=False
	return render_template("jouer.html",
							title="Jouer",
							session=session,
							url="jouer"
							)# <url> used for the grille template to know where to send the player's move


@app.route('/jouer/ia/')
@app.route('/jouer/ia/<int:col>')
def jouer_ia(col=None,IA=None):
	if 'grille' in session:
		grille=jsonpickle.decode(session['grille'])			
		if (grille.joueur1.nom!="IA" and grille.joueur2.nom!="IA") or grille.puissance4() or grille.estRemplie():
			session.pop('grille')
		elif col is not None and col < grille.max_column:
			if grille.joueur1.nom == "IA":
				joueur = grille.joueur2
				ia = grille.joueur1
			else:
				joueur = grille.joueur1
				ia = grille.joueur2
			if joueur.peutJouer(grille):
				joueur.jouer(grille,Jeton(joueur,col))
			if ia.peutJouer(grille):
				ia.jouer(grille)
			session['grille']=jsonpickle.encode(grille)
	if 'grille' not in session:
		if IA == None:
			return render_template("home.html",
							title="Accueil"
							)
		from random import randint
		joueur=Joueur("Player 1")
		ia=IA
		if randint(0,1) == 0:
			grille=Grille(joueur1=joueur, joueur2=ia)
		else:
			grille=Grille(joueur1=ia, joueur2=joueur)
			ia.jouer(grille)
		session['grille']=jsonpickle.encode(grille)
	return render_template("jouer.html",
							title="Jouer",
							session=session,
							url="jouer_ia"
							)

@app.route('/jouer/ia/tres_facile/')
def jouer_ia_tres_facile():
	from ia import IARandom
	if 'grille' in session:
		session.pop('grille')
	return jouer_ia(IA=IARandom("IA"))

@app.route('/jouer/ia/facile/')
def jouer_ia_facile():
	from ia import IAeasy
	if 'grille' in session:
		session.pop('grille')
	return jouer_ia(IA=IAeasy("IA"))
