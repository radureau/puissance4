from flask import Flask
app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'fb43c566-d92f-4c60-8127-bd9474f1ecc0'# $bash> uuidgen


app.config['BOOTSTRAP_SERVE_LOCAL']=True
from flask.ext.bootstrap import Bootstrap
Bootstrap(app)


from flask.ext.script import Manager
manager = Manager(app)

import os.path
def mkpath(p):
        return os.path.normpath(os.path.join(
                os.path.dirname(__file__),
                p))

from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = (
        'sqlite:///'+mkpath('../p4IA.db'))
db=SQLAlchemy(app)
